from turtle import *
import random
colors = ["red","orange","green","blue","yellow","light blue","purple"]
### Functions
# Modify code in this section
speed(0)
def star(size,star_color):
    color(star_color)
    begin_fill()
    for i in range(5):
        forward(size)
        left(72)
        forward(size)
        right(144)
    end_fill()


### The main code that gets run
# Do not modify anything after this line until exercise 7

for i in range(13):
    star(25,random.choice(colors))
    penup()
    right(360/13)
    forward(50)
    pendown()

done()
