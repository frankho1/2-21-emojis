from turtle import *
penup()
goto(-200,200)
pendown()
### Function that draws stars of different sizes
# Do not modify code in this section

def star(size,star_color):
    color(star_color)
    begin_fill()
    for i in range(5):
        forward(size)
        left(72)
        forward(size)
        right(144)
    end_fill()

### Main code area
# Modify code below this line

speed(0)
star(25,"red")
penup()
forward(70)
pendown()
star(50,"blue")
penup()
forward(140)
pendown()
star(100, "purple")

done()

