from turtle import *

speed(0)

def smile():
    fillcolor("yellow")
    begin_fill()
    circle(60)
    end_fill()

    penup()
    left(90)
    fd(85)
    left(90)
    fd(25)
    pendown()
    fillcolor("black")
    begin_fill()
    circle(10)
    end_fill()

    penup()
    right(180)
    fd(50)
    left(180)
    pendown()
    fillcolor("black")
    begin_fill()
    circle(10)
    end_fill()

    penup()
    fd(50)
    left(90)
    fd(45)
    pendown()
    circle(25,180)



def arrow():
    color("red")
    penup()
    right(90)
    fd(90)
    left(90)
    fd(70)
    right(90)
    begin_fill()
    pendown()
    fd(60)
    right(90)
    fd(100)
    right(90)
    fd(60)
    right(90)
    fd(100)
    end_fill()
    begin_fill()
    right(180)
    fd(100)
    right(90)
    fd(30)
    left(120)
    fd(120)
    left(120)
    fd(120)
    left(120)
    fd(30)

    end_fill()
    penup()

def money():
    speed(0)
    color("green")
    begin_fill()
    right(180)
    fd(60)
    pendown()
    left(90)
    fd(120)
    right(90)
    fd(300)
    right(90)
    fd(120)
    right(90)
    fd(300)
    end_fill()

    color("black")
    right(180)
    penup()
    fd(130)
    left(90)
    #fd(60)
    right(90)
    pendown()
    begin_fill()
    write("$", font= ("Arial", 100, "normal"))
    end_fill()
    penup()
    fd(250)
    right(90)
    fd(50)
    left(90)

    pendown()


def frown():
    speed(1)
    fillcolor("yellow")
    begin_fill()
    circle(60)
    end_fill()

    penup()
    left(90)
    fd(85)
    left(90)
    fd(25)
    pendown()
    fillcolor("black")
    begin_fill()
    circle(10)
    end_fill()

    penup()
    right(180)
    fd(50)
    left(180)
    pendown()
    fillcolor("black")
    begin_fill()
    circle(10)
    end_fill()

    penup()
    fd(50)
    left(90)
    fd(45)
    pendown()
    circle(25,-180)


smile()
arrow()
money()
frown()








done()


