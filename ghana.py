from turtle import *

### Functions for drawing
# Put the functions you write here



### Main code that gets run, to draw the german flag
# Replace this code with function calls!
def bar(bar_color):
    color(bar_color)
    begin_fill()
    forward(300)
    right(90)
    forward(50)
    right(90)
    forward(300)
    right(90)
    forward(50)
    right(90)
    end_fill()

def move_next():
    right(90)
    forward(50)
    left(90)

def star(size,star_color):
    color(star_color)
    begin_fill()
    for i in range(5):
        forward(size)
        left(72)
        forward(size)
        right(144)
    end_fill()

speed(0)
bar("red")
move_next()
bar("yellow")
move_next()
bar("green")
penup()
left(90)
forward(31)
right(90)
forward(125)
star(20,"black")




done()

    
